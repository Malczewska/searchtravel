export class Travel {
  idTravel: number;
  travelNumber: number;
  departureDate: Date;
  arrivalDate: Date;
  basePrice: number;
  freePlaces: number;
  from: string;
  to: string;
}
