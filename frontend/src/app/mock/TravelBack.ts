export class TravelBack {
  id: number;
  travelNumber: number;
  departureDate: string;
  arrivelDate: string;
  basePrice: number;
  freePlaces: number;
  from: string;
  to: string;

}
