export class TravelsInput {
  dateStart: Date;
  from: string;
  to: string;
}
