import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {SearchService} from '../service/search.service';
import {City} from '../mock/City';
import {Travel} from '../mock/Travel';
import {TravelsInput} from '../mock/TravelsInput';
import {parseTimelineCommand} from '@angular/animations/browser/src/render/shared';
import {ResultsComponent} from '../results/results.component';
import {TicketService} from '../service/ticket.service';
import {Ticket} from '../mock/Ticket';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})


export class HomeComponent implements OnInit {

myForm: FormGroup;
  constructor(
    private fb: FormBuilder,
    private searchService: SearchService,
    private ticketService: TicketService,
  ) {
  }

  source = new FormControl('', Validators.required);
  destination = new FormControl('', Validators.required);
  start = new FormControl('', Validators.required);
  cities: City[];
  travels: Travel[];
  selectedTravel: Travel;
  idTicketBuy: number;
  numberOfPlaces: number;
  reduction: number;
  price: number;
  dateTravel: Date;


  buy(travel: Travel) {
    this.selectedTravel = travel;
    const ticketInput = new Ticket();
    ticketInput.numberOfPlaces = 1;
    ticketInput.travelId = travel.idTravel;
    ticketInput.reductionId = 1;
    ticketInput.userId = 1;
    if (this.reduction) {
      this.price = (travel.basePrice / 2);
    } else {
      this.price = (travel.basePrice);
    }
    this.dateTravel = travel.departureDate;
    console.log(this.reduction);
    this.ticketService.getTicket(ticketInput).subscribe(s => this.idTicketBuy = s);
    console.log(this.idTicketBuy);
  }




  submit() {
    const tB = new TravelsInput();
    tB.dateStart = this.start.value;
    tB.from = this.source.value;
    tB.to = this.destination.value;
  this.searchService.getTravels(tB).subscribe(s => this.travels = s);

  }



  ngOnInit(): void {
    this.searchService.getCities().subscribe(s => this.cities = s);
  }


}

