import {Component, Input, OnInit} from '@angular/core';
import {Travel} from '../mock/Travel';
import {HomeComponent} from '../home/home.component';
import {SearchService} from '../service/search.service';
import {TravelsInput} from '../mock/TravelsInput';
import {Observable} from 'rxjs';


@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css']
})
export class ResultsComponent implements OnInit {
  @Input() travels: Travel[];
  home: HomeComponent;
  constructor(private searchService: SearchService) { }

  ngOnInit() {

  }
  sub(tb: TravelsInput) {
    this.searchService.getTravels(tb).subscribe(s => this.travels = s);
  }

}
