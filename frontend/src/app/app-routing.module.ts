import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './home/home.component';
import {ResultsComponent} from './results/results.component';
import {TravelComponent} from './travel/travel.component';

const routes: Routes = [
  {path: 'home', component: HomeComponent},
  {path: 'results', component: ResultsComponent},
  {path: '**', redirectTo: 'home'},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
