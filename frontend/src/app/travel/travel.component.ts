import {Component, OnInit} from '@angular/core';
import { Travel} from '../mock/Travel';
import {SearchService} from '../service/search.service';
import {City} from '../mock/City';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-travel',
  templateUrl: './travel.component.html',
  styleUrls: ['./travel.component.css']
})
export class TravelComponent implements OnInit {


  travel: Travel[];
  cities: City[];
mycontrol: FormControl;
  constructor(private searchService: SearchService) { }


  ngOnInit() {
     this.searchService.getCities().subscribe( s => this.cities = s);
  }

}
