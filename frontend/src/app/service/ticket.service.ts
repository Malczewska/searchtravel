import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Ticket} from '../mock/Ticket';
import {Observable} from 'rxjs';
import {apiUrl} from '../config';

@Injectable({
  providedIn: 'root'
})
export class TicketService {

  ticketUrl = 'buy';

  constructor(private http: HttpClient) { }


  getTicket(ticket: Ticket): Observable<number> {
    return this.http.post<number>(apiUrl + this.ticketUrl, ticket);

  }
}
