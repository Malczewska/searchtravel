import { Injectable } from '@angular/core';
import { City} from '../mock/city';
import { Observable, of } from 'rxjs';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Travel} from '../mock/Travel';
import {TravelsInput} from '../mock/TravelsInput';
import {apiUrl} from '../config';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor(private http: HttpClient) { }

  travelUrl = 'travels';
  citiesUrl = 'allcities';
  destUrl = 'dest';


  getTravels(travelsInput: TravelsInput): Observable<Travel[]> {
    return this.http.post<Travel[]>(apiUrl + this.travelUrl, travelsInput);
  }

  getCities(): Observable<City[]> {
    return this.http.get<City[]>(apiUrl + this.citiesUrl);
  }
  getDestination(dest: string): Observable<String[]> {
    const params = new HttpParams().set('dest', dest);
    return this.http.get<String[]>(apiUrl + this.destUrl, {params});
  }

}
