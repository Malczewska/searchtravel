package com.wat.ai.dal1.ticket;


import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "MULTIPLIER")
@Data
public class Multiplier {
    @Id
    @Column(name="ID")
    private Long id;

    @Column(name="MULTIPLIER")
    private Float multiplier;

}
