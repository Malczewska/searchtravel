package com.wat.ai.dal1.travel;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface TravelRepository extends JpaRepository<Travel, Integer> {
    List<Travel> findAll();


    @Query("select t from Travel t, City c1, City c2 where " +
            "t.departureDate > :dateStart " +
            "and t.freePlaces > 0 " +
            "and t.city1 = c1.id " +
            "and t.city2 = c2.id " +
            "and c1.nameCity= :city1 " +
            "and c2.nameCity= :city2 ")
    List<Travel> findtravels(@Param("dateStart") Date dateStart,
                             @Param("city1") String city1,
                             @Param("city2") String city2);

    List<Travel> findTravelById(Long id);
/*
    @Query("select distinct t  from Travel t where " +
            "t.city1 = :src ")
    List<Integer> findDest(@Param("src") Integer src);
*/
    @Query("select distinct c2.nameCity  from Travel t, City c1, City c2 where " +
            "t.city1 = c1.id " +
            "and t.city2 = c2.id " +
            "and c1.nameCity = :src")
    List<String> findDest(@Param("src") String src);

    @Query("select distinct c1.nameCity  from Travel t, City c1, City c2 where " +
            "t.city1 = c1.id " +
            "and t.city2 = c2.id " +
            "and c2.nameCity = :dest")
    List<String> findSrc(@Param("dest") String dest);

}
