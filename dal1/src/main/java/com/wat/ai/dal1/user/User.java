package com.wat.ai.dal1.user;


import lombok.Builder;
import lombok.Data;
import com.wat.ai.dal1.enums.Role;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "USER1")
@Data
@Builder
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_USER")
    @SequenceGenerator(name = "SEQ_USER", sequenceName = "SEQ_USER")
    @Column(name="ID")
    private Integer id;

    @Column(name="USERNAME")
    private String username;

    @Column(name="PASSWORD")
    private String password;

    @Column(name="FIRSTNAME")
    private String firstname;

    @Column(name="LASTNAME")
    private String lastname;

    @Column(name="EMAIL")
    private String email;

    @Enumerated(EnumType.STRING)
    @Column(name = "ROLE")
    private Role role;
}
