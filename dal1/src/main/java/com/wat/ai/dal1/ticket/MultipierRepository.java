package com.wat.ai.dal1.ticket;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MultipierRepository extends JpaRepository<Multiplier, Integer> {
}