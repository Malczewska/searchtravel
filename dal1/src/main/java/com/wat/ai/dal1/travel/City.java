package com.wat.ai.dal1.travel;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "CITY")
@Data
public class City {

    @Id
    @Column(name="ID")
    private Integer id;

    @Column(name="NAME_CITY")
    private String nameCity;
}
