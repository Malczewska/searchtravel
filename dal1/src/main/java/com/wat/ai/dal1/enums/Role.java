package com.wat.ai.dal1.enums;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

public enum Role {
    USER("user"), ADMIN("admin");

    private String name;

    Role(String name){
        this.name = name;
    }

    @Enumerated(EnumType.STRING)
    public String getName() {
        return name;
    }
}
