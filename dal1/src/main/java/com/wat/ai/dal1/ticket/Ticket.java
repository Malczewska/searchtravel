package com.wat.ai.dal1.ticket;

import com.wat.ai.dal1.travel.Travel;
import com.wat.ai.dal1.user.User;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "TICKET")
@Data
public class Ticket {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_TICKET")
    @SequenceGenerator(name = "SEQ_TICKET", sequenceName = "SEQ_TICKET")
    @Column(name = "ID")
    private Integer id;

    @Column(name = "NUMBER_OF_PLACES")
    private Integer numberOfPlaces;
    @Column(name = "TRAVEL_ID")
    private Integer travelId;
    @Column(name = "USER1_ID")
    private Integer userId;
    @Column(name = "REDUCTION_ID")
    private Integer reductionId;

}
