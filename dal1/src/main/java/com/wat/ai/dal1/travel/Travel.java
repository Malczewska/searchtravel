package com.wat.ai.dal1.travel;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "TRAVEL")
@Data
public class Travel {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_TRAVEL")
    @SequenceGenerator(name = "SEQ_TRAVEL", sequenceName = "SEQ_TRAVEL")
    @Column(name="ID")
    private Integer id;

    @Column(name="TRAVEL_NUMBER")
    private Integer travelNumber;

    @Column(name="DEPARTURE_DATE")
    private Date departureDate;

    @Column(name="ARRIVAL_DATE")
    private Date arrivalDate;

    @Column(name="BASE_PRICE")
    private Integer basePrice;

    @Column(name="FREE_PLACES")
    private Integer freePlaces;

    @Column(name="CITY1_ID")
    private Integer city1;

    @Column(name="CITY2_ID")
    private Integer city2;


}
