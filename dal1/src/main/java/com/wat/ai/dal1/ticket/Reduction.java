package com.wat.ai.dal1.ticket;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "REDUCTION")
@Data
public class Reduction {

    @Id
    @Column(name="ID")
    private Integer id;

    @Column(name="NAME")
    private String name;

    @ManyToOne
    private Multiplier multiplier;
}
