package com.wat.ai.dal1.ticket;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface TicketRepository extends JpaRepository<Ticket, Integer> {

    @Query("select t.id from Ticket t where " +
            "t.travelId = :travelId")
    Integer findTicketBy(@Param("travelId") Integer travelId);
}
