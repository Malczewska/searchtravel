package com.wat.ai.bll.service.city;

import com.wat.ai.bll.model.city.CityDTO;
import com.wat.ai.bll.service.MainAssembler;
import com.wat.ai.dal1.travel.City;

import org.springframework.stereotype.Service;

@Service
public class CityAssemblerGeneral extends MainAssembler<City, CityDTO> {

    @Override
    public CityDTO toDto(City city) {
        return CityDTO.builder()
                .id(city.getId())
                .nameCity(city.getNameCity())
                .build();

    }

}
