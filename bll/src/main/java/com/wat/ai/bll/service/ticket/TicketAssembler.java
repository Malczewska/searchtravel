package com.wat.ai.bll.service.ticket;

import com.wat.ai.bll.model.ticket.TicketDTO;
import com.wat.ai.bll.service.MainAssembler;
import com.wat.ai.dal1.ticket.Ticket;
import org.springframework.stereotype.Service;

@Service
public class TicketAssembler extends MainAssembler<Ticket, TicketDTO> {
    @Override
    public TicketDTO toDto(Ticket ticket){
        return TicketDTO.builder()
                .id(ticket.getId())
                .build();
    }

}
