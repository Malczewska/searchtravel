package com.wat.ai.bll.service.city;


import com.wat.ai.bll.model.city.CityDTO;
import com.wat.ai.dal1.travel.City;
import com.wat.ai.dal1.travel.CityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CityService {

    @Autowired
    private CityRepository cityRepository;

    @Autowired
    private CityAssemblerDetailed cityAssemblerDetailed;

    public List<CityDTO> getCities(){
        List<City> all = cityRepository.findAll();
        return cityAssemblerDetailed.toDtoList(all);
    }


}
