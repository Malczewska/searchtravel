package com.wat.ai.bll.model.user;


import lombok.Data;
import com.wat.ai.dal1.enums.Role;



@Data
public class UserLoginOut {
    private String token;
    private Long userId;
    private String firstname;
    private Role role;
}
