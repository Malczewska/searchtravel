
package com.wat.ai.bll.model.user;
import lombok.Data;


@Data
public class UserLoginIn {
    private String username;
    private String password;
}
