package com.wat.ai.bll.model.travel;


import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
public class TravelDTO {
    private Integer idTravel;
    private Integer travelNumber;
    private Date departureDate;
    private Date arrivalDate;
    private Integer basePrice;
    private Integer freePlaces;
    private String to;
    private String from;

}
