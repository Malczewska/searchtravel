package com.wat.ai.bll.service.travel;


import com.wat.ai.bll.model.travel.TravelDTO;
import com.wat.ai.bll.model.travel.*;

import com.wat.ai.dal1.travel.Travel;
import com.wat.ai.dal1.travel.TravelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class TravelService {

    @Autowired
    private TravelRepository travelRepository;

    @Autowired
    private TravelAssembler travelAssembler;


    public List<TravelDTO> getTravels() {
        List<Travel> all = travelRepository.findAll();
        return travelAssembler.toDtoList(all);
    }


    public List<TravelDTO> getTravels(TravelsInput travelsInput) {
        List<Travel> all_travel = travelRepository.findtravels(
                travelsInput.getDateStart(),
                travelsInput.getFrom(),
                travelsInput.getTo());
        return travelAssembler.toDtoList(all_travel);
    }


   public List<String> getPossibleSources(String dest) {
       List<String> possibleSrc = travelRepository.findSrc(dest);
       return possibleSrc;
   }
    public List<String> getPossibleDestinations(String src) {
        List<String> possibleDest= travelRepository.findDest(src);
        return possibleDest;
    }


}
