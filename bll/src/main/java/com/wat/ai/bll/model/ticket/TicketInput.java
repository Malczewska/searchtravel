package com.wat.ai.bll.model.ticket;



import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TicketInput {

    private Integer numberOfPlaces;
    private Integer travelId;
    private Integer reductionId;
    private Integer userId;
}
