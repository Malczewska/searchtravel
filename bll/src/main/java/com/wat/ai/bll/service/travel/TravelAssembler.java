package com.wat.ai.bll.service.travel;

import com.wat.ai.bll.model.travel.TravelDTO;
import com.wat.ai.bll.service.MainAssembler;
import com.wat.ai.dal1.travel.Travel;
import org.springframework.stereotype.Service;

@Service
public class TravelAssembler extends MainAssembler<Travel, TravelDTO> {

    @Override
    public TravelDTO toDto(Travel travel){
        return TravelDTO.builder()
                .idTravel(travel.getId())
                .travelNumber(travel.getTravelNumber())
                .departureDate(travel.getDepartureDate())
                .arrivalDate(travel.getArrivalDate())
                .basePrice(travel.getBasePrice())
                .freePlaces(travel.getFreePlaces())
                .build();
    }
}
