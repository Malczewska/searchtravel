package com.wat.ai.bll.service.ticket;

import com.wat.ai.bll.model.ticket.TicketInput;
import com.wat.ai.dal1.ticket.Ticket;
import com.wat.ai.dal1.ticket.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class TicketService {

    @Autowired
    TicketRepository ticketRepository;

    public Integer addTicket(TicketInput ticketInput) {
       Ticket t = new Ticket();
       int x;
        t.setNumberOfPlaces(ticketInput.getNumberOfPlaces());
        t.setTravelId(ticketInput.getTravelId());
        t.setReductionId(ticketInput.getReductionId());
        t.setUserId(ticketInput.getUserId());
        ticketRepository.save(t);
        Random r= new Random();
        x = r.nextInt(50);
        return x;

    }
}
