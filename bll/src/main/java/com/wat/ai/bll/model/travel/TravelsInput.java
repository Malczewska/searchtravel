package com.wat.ai.bll.model.travel;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TravelsInput {
    private Date dateStart;
    private String from;
    private String to;

}
