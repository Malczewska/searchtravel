package com.wat.ai.bll.model.ticket;


import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TicketDTO {
    private Integer id;
}
