package com.wat.ai.bll.model.city;


import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CityDTO {
    private Integer id;
    private String nameCity;

}
