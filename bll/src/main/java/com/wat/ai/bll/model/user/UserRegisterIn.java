package com.wat.ai.bll.model.user;

import lombok.Data;


@Data
public class UserRegisterIn {
    private String username;
    private String password;
    private String firstname;
    private String lastname;
    private String email;

}
