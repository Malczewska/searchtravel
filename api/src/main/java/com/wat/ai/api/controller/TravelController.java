package com.wat.ai.api.controller;
import com.wat.ai.bll.model.city.CityDTO;
import com.wat.ai.bll.model.travel.TravelDTO;
import com.wat.ai.bll.model.travel.TravelsInput;
import com.wat.ai.bll.service.city.CityService;
import com.wat.ai.bll.service.travel.TravelService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping(value = "/api")
public class TravelController {

    @Autowired
    private TravelService travelService;

    @Autowired
    private CityService cityService;

    @RequestMapping(value = "/allcities", method = RequestMethod.GET)
    public List<CityDTO> getCities() {
        return cityService.getCities();
    }

    @RequestMapping(value = "/dest", method = RequestMethod.GET)
    public List<String> getPossibleDestinations(@RequestParam String src) throws IOException {
        return travelService.getPossibleDestinations(src);
    }

   @RequestMapping(value = "/src", method = RequestMethod.GET)
    public List<String> getPossibleSources(@RequestParam String dest) {
        return travelService.getPossibleSources(dest);
    }



    @RequestMapping(value = "/travels", method = RequestMethod.POST)
    public List<TravelDTO> getTravels(@RequestBody TravelsInput travelsInput) {
        return travelService.getTravels(travelsInput);
    }
}


