package com.wat.ai.api.controller;

import com.wat.ai.bll.model.ticket.TicketInput;
import com.wat.ai.bll.service.ticket.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(value = "/api")
public class TicketController {

    @Autowired
    private TicketService ticketService;


    @RequestMapping(value = "/buy", method = RequestMethod.POST)
    public Integer add(@RequestBody TicketInput ticketInput) {
        return ticketService.addTicket(ticketInput);
    }

}
