package com.wat.ai.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@ComponentScan("com.wat.ai")
@EntityScan("com.wat.ai")
@EnableJpaRepositories("com.wat.ai")
@SpringBootApplication
public class BusApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(BusApplication.class, args);
    }
}

